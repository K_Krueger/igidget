package com.example.igidget;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class son extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_son);
    }

    public void List1(View view){
        Intent List1 = new Intent(this, List1.class);
        startActivity(List1);
    }

    public void List2(View view){
        Intent List2 = new Intent(this, List2.class);
        startActivity(List2);
    }

    public void List3(View view){
        Intent List3 = new Intent(this, List3.class);
        startActivity(List3);
    }

    public void List4(View view){
        Intent List4 = new Intent(this, List4.class);
        startActivity(List4);
    }
}
