package com.example.igidget;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

//All images for main screen sourced from https://crossings.church/staff/

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void dad(View view){
        Intent dad = new Intent(this, dad.class);
        startActivity(dad);
    }

    public void mom(View view){
        Intent mom = new Intent(this, mom.class);
        startActivity(mom);
    }

    public void son(View view){
        Intent son = new Intent(this, son.class);
        startActivity(son);
    }

    public void daughter(View view){
        Intent daughter = new Intent(this, daughter.class);
        startActivity(daughter);
    }
}
